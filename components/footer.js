import Container from './container'
import { EXAMPLE_PATH } from '@/lib/constants'

export default function Footer() {
  return (
    <footer className="bg-accent-1 border-t border-accent-2">
      <Container>
        <div className="py-28 flex flex-col lg:flex-row items-center">
          <div className="mx-3 font-bold hover:underline">
            Copyright © 2020 Shivam/Dev Stack. All rights reserved.
          </div>
          <div className="flex flex-col lg:flex-row justify-center items-center lg:pl-4 lg:w-1/2">
            <a
              href="https://www.linkedin.com/in/kshivam-kumar/"
              className="mx-3 bg-black hover:bg-white hover:text-black border border-black text-white font-bold py-3 px-12 lg:px-8 duration-200 transition-colors mb-6 lg:mb-0"
            >
              Contact Me
            </a>
            <a
              href="https://github.com/kumarshivam12"
              className="mx-3 font-bold hover:underline"
            >
              My Projects
            </a>
          </div>
        </div>
      </Container>
    </footer>
  )
}
